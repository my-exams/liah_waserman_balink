import {useState} from "react"
import {H1} from "../Components/H1.style"
import {AppContainer} from "../Components/Container.style"
import {Button} from "../Components/Button.style"
import {Input} from "../Components/Input.style"
import {Label} from "../Components/Input.style";
import logo from "../images/user.png"
import {Img} from "../Components/Img.style"
import {Span} from "../Components/Span.style"
import axios from "axios"

function UserComp(props) {
    const [user, setUser] = useState(props.user)

    const [userValidation, setUserValidation] = useState({
        "firstName": !props.edit,
        "lastName": !props.edit,
        "age": !props.edit,
        "phone": !props.edit
    })
    const [userValiditionMassages, setUserValidationMassages] = useState({
        "firstName": "* Required filed",
        "lastName": "* Required filed",
        "age": "* Required filed",
        "phone": "* Required filed"
    })
    const handleInput = (e) => {
        let value = e.target.value
        setUser({...user, [e.target.name]: value})
        if (e.target.value.trim() === "" || e.target.value === undefined) {
            setUserValidation({...userValidation, [e.target.name]: true})
            setUserValidationMassages({...userValiditionMassages, [e.target.name]: "* Required filed"})
        } else {
            setUserValidation({...userValidation, [e.target.name]: false})
            if (e.target.name === "age" && e.target.value < 0) {
                setUserValidation({...userValidation, [e.target.name]: true})
                setUserValidationMassages({...userValiditionMassages, age: "* Contains only positive value"})
            }
            if (e.target.name === "phone") {
                const regExp = /^\(?([0-9]{10})$/
                setUserValidation({...userValidation, [e.target.name]: true})
                console.log(e.target.value.match(regExp))
                if (e.target.value.match(regExp) === null && (e.target.value !== undefined || e.target.value.trim() !== "")) {
                    setUserValidationMassages({...userValiditionMassages, phone: "* contains only 10 digits"})
                } else {
                    setUserValidation({...userValidation, [e.target.name]: false})
                }
            }


        }
    }

    const updateUser = async (e) => {
        console.log('edit')
        console.log(e)
        if (props.edit) {
            console.log('update')
            try {
                await axios.put('http://localhost:8000/user/' + user.id, user)
                props.callback(true)
            } catch (err) {
                console.log('Error Message: ', err.response.data.message);
                alert(err.response.data.message)
            }
        } else {
            console.log('add')
            console.log(user)
            try {
                await axios.post('http://localhost:8000/user', user)
                props.callback(true)
            } catch (err) {
                console.log('Error Message: ', err.response.data.message);
                alert(err.response.data.message)
            }
        }

    }
    return <div>
        <H1>{props.edit ? 'Edit User' : 'Add User'}</H1>
        <AppContainer textAlign="center" margin="0% 15%">
            {props.edit ?
                <AppContainer textAlign="left" margin="3% 38%">
                    <Img src={logo} alt="Logo"/><b><Label>{user.firstName} {user.lastName}</Label></b>
                </AppContainer> : null
            }
            <AppContainer margin="3% 35%">
                <Input type="text" placeholder="First Name" name="firstName" onChange={handleInput}
                       value={user.firstName} width="80%"></Input><br></br>
                {(userValidation.firstName) &&
                    <div><Span color="red" size="13px">{userValiditionMassages.firstName}</Span><br></br><br></br>
                    </div>}{(!userValidation.firstName) && <br></br>}
                <Input type="text" placeholder="Last Name" name="lastName" onChange={handleInput} value={user.lastName}
                       width="80%"></Input><br></br>
                {(userValidation.lastName) &&
                    <div><Span color="red" size="13px">{userValiditionMassages.lastName}</Span><br></br><br></br>
                    </div>}{(!userValidation.lastName) && <br></br>}
                <Input type="number" placeholder="Age" name="age" onChange={handleInput} value={user.age}
                       width="80%"></Input><br></br>
                {(userValidation.age) &&
                    <div><Span color="red" size="13px">{userValiditionMassages.age}</Span><br></br><br></br>
                    </div>}{(!userValidation.age) && <br></br>}
                <Input type="text" placeholder="Phone" name="phone" onChange={handleInput} value={user.phone}
                       width="80%"></Input><br></br>
                {(userValidation.phone) && <Span color="red" size="13px">{userValiditionMassages.phone}</Span>}
            </AppContainer>
            <AppContainer margin="3% 25%">
                <Button radius="8px" height="30px" width="45%" float="left" backgroundColor="#080536"
                        onClick={updateUser}
                        disabled={userValidation.firstName || userValidation.lastName || userValidation.age || userValidation.phone}>{props.edit ? 'Edit' : 'Add'}</Button>
                <Button radius="8px" height="30px" width="45%" float="right" backgroundColor="#780527"
                        onClick={() => props.callback(true)}>Back</Button>
            </AppContainer>
        </AppContainer>
    </div>
}

export default UserComp