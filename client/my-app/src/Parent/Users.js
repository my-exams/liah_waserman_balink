import {useState, useEffect} from 'react'
import axios from "axios"
import {H1} from "../Components/H1.style"
import {Table, Th, Tr, Td} from "../Components/Table.style"
import {AppContainer} from "../Components/Container.style"
import {Button} from "../Components/Button.style"
import {Input} from "../Components/Input.style"
import UserComp from "../Child/User"

function UsersComp() {
    const [users, setUsers] = useState([])
    const [search, setSearch] = useState("Search id...")
    const [user, setUser] = useState({})
    const [view, setView] = useState(true)
    const [edit, setEdit] = useState(false)
    const [viewFilter, setViewFilter] = useState({"key": "id", "view": false})
    let backgroundColor


    useEffect(() => {
        console.log("useEffect")
        getData()
    }, [view])

    const getData = async () => {
        try {
            const {data} = await axios.get('http://localhost:8000/user')
            setUsers(data)
        } catch (err) {
            console.log('Error Message: ', err.response.data.message);
            alert(err.response.data.message)
        }

    }

    const addUser = () => {
        setUser({})
        setView(false)
        setEdit(false)
    }

    const getUser = (user) => {
        setView(false)
        setUser(user)
        setEdit(true)
    }
    const filterByKey = (key, value) => {
        console.log(key);
        console.log(value);
        const filtered = [...users].filter((a) => {
            return a[key].toString().includes(value)
        })
        setUsers(filtered)
        console.log(filtered)
    }

    const filtered = (key) => {
        let str = key
        if (key === "firstName") {
            str = "first name"
        }
        if (key === "lastName") {
            str = "last name"
        }
        setSearch(`Search ${str}...`)
        setViewFilter({"key": key, "view": true})
    }

    const handleInput = (e) => {
        if (e.target.value !== "") {
            filterByKey(viewFilter.key, e.target.value)
        } else {
            getData()
        }


    }

    const sortByKey = (key) => {
        console.log(key)
        let sorted = []
        sorted = [...users].sort((a, b) => {
            const x = a[key]
            const y = b[key]
            if (key === "age" || key === "id") {
                return x - y
            }
            return (x < y) ? -1 : ((x > y) ? 1 : 0);
        })
        setUsers(sorted)
    }


    const deleteUser = async (user) => {
        try {
            await axios.delete('http://localhost:8000/user/' + user.id)
            setView(true)
        } catch (err) {
            console.log('Error Message: ', err.response.data.message);
            alert(err.response.data.message)
        }
    }

    if (view) {
        return <div>
            <H1>Home</H1>
            <AppContainer margin="0% 15%">
                <AppContainer margin="2% 35%">
                    <Input onKeyUp={handleInput} placeholder={search} width="100%"></Input></AppContainer>
                <Table>
                    <thead>
                    <Tr>
                        <Th name="id" onDoubleClick={() => sortByKey("id")} onClick={() => filtered("id")}>ID</Th>
                        <Th name="firstName" onDoubleClick={() => sortByKey("firstName")}
                            onClick={() => filtered("firstName")}>First Name</Th>
                        <Th name="lastName" onDoubleClick={() => sortByKey("lastName")}
                            onClick={() => filtered("lastName")}>Last Name</Th>
                        <Th name="age" onDoubleClick={() => sortByKey("age")} onClick={() => filtered("age")}>Age</Th>
                        <Th name="phone" onDoubleClick={() => sortByKey("phone")}
                            onClick={() => filtered("phone")}>Phone</Th>
                        <Th></Th>
                    </Tr>
                    </thead>
                    <tbody>
                    {users.map((user, index) => {
                        if (index % 2 === 0) {
                            backgroundColor = "white"
                        } else {
                            backgroundColor = " #dddddd"
                        }
                        return <Tr onClick={() => getUser(user)} key={index} backgroundColor={backgroundColor}>
                            <Td>{user.id}</Td>
                            <Td>{user.firstName}</Td>
                            <Td>{user.lastName}</Td>
                            <Td>{user.age}</Td>
                            <Td>{user.phone}</Td>
                            <Td margin="5% 0%"><Button radius="8px" height="20px" width="60px" float="center"
                                                       backgroundColor="#780527"
                                                       onClick={() => deleteUser(user)}>Delete</Button></Td>
                        </Tr>
                    })}
                    </tbody>
                </Table><br></br>
                <Button radius="50%" height="30px" width="30px" float="right" backgroundColor="#080536"
                        onClick={addUser}>+</Button>
            </AppContainer>
        </div>
    }
    return <UserComp callback={setView} user={user} edit={edit}></UserComp>

}

export default UsersComp