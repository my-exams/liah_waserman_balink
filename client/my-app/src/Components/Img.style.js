import styled from "styled-components"

export const Img = styled.img`
  width: 100px;
  height: 100px;
  text-align: center;
  opacity: 0.9;
`