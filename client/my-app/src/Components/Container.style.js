import styled from "styled-components"

export const AppContainer = styled.div`
  font-family: arial, sans-serif;
  border-collapse: collapse;
  text-align: ${(props) => props.textAlign};
  margin: ${(props) => props.margin};
`