import styled from "styled-components"

export const Button = styled.button`
  color: white;
  background-color: ${(props) => props.backgroundColor};
  height: ${(props) => props.height};
  width: ${(props) => props.width};
  font-weight: bold;
  float: ${(props) => props.float};
  border-radius: ${(props) => props.radius};
`
