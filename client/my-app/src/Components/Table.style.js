import styled from "styled-components"

export const Table = styled.table`
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%
`
export const Td = styled.td`
  text-align: left;
  font-size: 10pt;
  padding: 8px;
  color: #4a4848;
`
export const Th = styled.th`
  text-align: left;
  padding: 8px;
  color: white;
  font-size: 10pt;
  background-color: #080536;
`
export const Tr = styled.tr`
  background-color: ${(props) => props.backgroundColor};;
`

