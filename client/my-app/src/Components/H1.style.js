import styled from "styled-components"

export const H1 = styled.h1`
  margin: 5% 10%;
  color: #080536;
`
