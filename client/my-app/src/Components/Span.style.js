import styled from "styled-components"

export const Span = styled.span`
  color: ${(props) => props.color};
  font-size: ${(props) => props.size};
  float:left;
  margin: 0% 8%;
`