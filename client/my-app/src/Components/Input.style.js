import styled from "styled-components"

export const Input = styled.input`
  height: 30px;
  width: ${(props) => props.width};;
  font-weight: bold;
  float: center;
  border-radius: 8px;
  border: 3px solid #ccc;

`
export const Label = styled.label`
  color: #080536;
`