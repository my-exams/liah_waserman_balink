const jsonfile = require('jsonfile');
const fs = require('fs');
const utils = require('../lib/utils')
const config = require('config')
const Ajv = require("ajv")
const ajv = new Ajv()
const userDataSchema = require("./userDataSchema.json")
const dataFilePath = __dirname + config.dataFilePath

const getUsers = () => {
    return new Promise((resolve, reject) => {

        fs.readFile(dataFilePath, (err, data) => {
            if (err) {
                if (err.code === "ENOENT") { // handle the case when users file doesn't exist
                    fs.openSync(dataFilePath, 'w')
                    resolve([])
                } else { // handle general errors
                    reject({"message": "Internal Server Error"})
                }
            } else {
                const userData = data.toString().trim();
                if (!userData.length) { // handle an empty file
                    resolve([])
                } else {
                    try {
                        resolve(JSON.parse(userData))
                    } catch (e) {
                        //return error if user data file is corrupted
                        reject({"message": "invalid user data file"})
                    }

                }
            }
        })
    })
}

const addUser = async (userData) => {
    const users = await getUsers()
    return new Promise(async (resolve, reject) => {
        const isValidSchema = ajv.validate(userDataSchema, userData)
        if (!isValidSchema) {
            const errMessage = `field ${ajv.errors[0].instancePath.substring(1)} ${ajv.errors[0].message}`
            reject({"message": errMessage})
        }
        userData.age = userData.age.toString()
        const {isDuplicated, userId} = await utils.isDuplicatedUser(userData, users);
        if (!isDuplicated) {
            const id = utils.getMaxUserId(users).toString()
            console.log(typeof (id))
            userData.id = id
            users.push(userData)
            jsonfile.writeFile(dataFilePath, users, (err) => {
                if (err) {
                    reject({"message": "Internal Server Error"})
                }
                resolve(userData)
            })
        } else {
            reject({"message": 'duplicated user'})
        }
    })
}

const updateUser = async (id, userData) => {
    const users = await getUsers()
    return new Promise(async (resolve, reject) => {
        const isValidSchema = ajv.validate(userDataSchema, userData)
        if (!isValidSchema) {
            const errMessage = `field ${ajv.errors[0].instancePath.substring(1)} ${ajv.errors[0].message}`
            reject({"message": errMessage})
        }
        const userIndex = await utils.findUserById(id, users)
        if (userIndex !== undefined) {
            const {isDuplicated, userId} = await utils.isDuplicatedUser(userData, users);
            if (isDuplicated && userId !== id) {
                reject({"message": 'duplicated user'})
            }
            users[userIndex].id = id
            users[userIndex].firstName = userData.firstName
            users[userIndex].lastName = userData.lastName
            users[userIndex].age = userData.age.toString()
            users[userIndex].phone = userData.phone
            jsonfile.writeFile(dataFilePath, users, (err) => {
                if (err) {
                    reject({"message": "Internal Server Error"})
                }
                resolve(userData)
            })
        } else {
            reject({"message": `user with id=${id} not found`})
        }
    })
}

const deleteUser = async (id) => {
    const users = await getUsers()
    return new Promise(async (resolve, reject) => {
        const userIndex = await utils.findUserById(id, users)
        if (userIndex !== undefined) {
            users.splice(userIndex, 1);
            jsonfile.writeFile(dataFilePath, users, (err) => {
                if (err) {
                    reject({"message": "Internal Server Error"})
                }
                resolve({"message": id})
            })
        } else {
            reject({"message": `user with id=${id} not found`})
        }
    })
}

module.exports = {getUsers, addUser, updateUser, deleteUser}