const express = require("express")
const router = express.Router()
const usersDAL = require('../data-layer/userManager')
const logger = require('../lib/logger').log

router.route('/').get(
    async function (req, res) {
        usersDAL.getUsers()
            .then(response => {
                return res.json(response)
            })
            .catch(function (err) {
                logger.error(err)
                return res.status(404).send(err)
            })
    })

router.route('/').post(
    async function (req, res) {
        const user = req.body
        usersDAL.addUser(user)
            .then(response => {
                return res.json(response)
            })
            .catch(function (err) {
                logger.error(err)
                return res.status(404).send(err)
            })
    }
)
router.route('/:id').put(
    async function (req, res) {
        const user = req.body
        const id = req.params.id
        usersDAL.updateUser(id, user)
            .then(response => {
                return res.json(response)
            })
            .catch(function (err) {
                logger.error(err)
                return res.status(404).send(err)
            })
    }
)

router.route('/:id').delete(
    async function (req, res) {
        const id = req.params.id
        usersDAL.deleteUser(id)
            .then(response => {
                return res.json(response)
            })
            .catch(function (err) {
                logger.error(err)
                return res.status(404).send(err)
            })
    }
)

module.exports = router