// auxiliary function to find user with max id
function getMaxUserId(users) {
    const user = users.length ? users.reduce((prev, current) => (prev.id > current.id) ? prev : current) : undefined
    return user ? parseInt(user.id) + 1 : 1
}

// auxiliary function to find user by id
async function findUserById(id, users) {
    let userIndex = undefined
    users.find((user, index) => {
        if (user.id === id) {
            userIndex = index
        }
    })
    return userIndex
}

// auxiliary function to check whether user is duplicated
async function isDuplicatedUser(userData, users) {
    let isDuplicated = false;
    let userId = -1;
    users.find(user => {
        // we use == and not === for comparison to ensure that the same integer and string will be compared,
        // e.g. 20 and "20" are the same
        if (user.firstName === userData.firstName &&
            user.lastName === userData.lastName &&
            user.age === userData.age &&
            user.phone === userData.phone) {
            isDuplicated = true;
            userId = user.id;
        }
    })
    return {isDuplicated, userId}
}

module.exports = {findUserById, isDuplicatedUser, getMaxUserId}