const bunyan = require('bunyan')
const config = require('config')
const log = bunyan.createLogger({name: config.server.name})

module.exports = {log}