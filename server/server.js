const express = require('express')
const config = require('config');
const createError = require('http-errors');
const userRouter = require('./routers/userRouter');
const morgan = require('morgan');
const logger = require('./lib/logger').log

// Allow Cors
const cors = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Authorization");
    res.header("Access-Control-Allow-Credentials", true);
    if ('OPTIONS' === req.method) {
        res.send(200);
    } else {
        next();
    }
}

const port = config.server.port

const app = express()
app.use(morgan('dev')); //log HTTP request + exec time
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors);

// This block is required to upload automatcally REACT client homepage
if (config.server.useStaticClientPage) {
    const clientPath = '../client/my-app/build'
// add REACT client path to server
    app.use(express.static(clientPath))
// upload client homepage
    app.get('/', function (req, res) {
        res.sendFile(clientPath + 'index.html')
    })
}
// handle API routes
app.use('/user', userRouter)

//unhandled API
app.use(function (req, res, next) {
    const err = createError(404)
    logger.error(err.message)
    next(err);
})

//handle internal server error
app.use(function (err, req, res, next) {
    logger.error(err)
    res.status(err.status || 500);
    res.send(err);
})

app.listen(port, () => {
    logger.info(`server is started and can be accessed at http://localhost:${port}`);
})