# The instructions to run the client and the server on Windows 10.
1. Run **start.bat** from the root folder (**liah_waserman_balink**)
2. Wait until batch file will finish to run and server logs will be displayed
3. Go to the browser (chrome is preferable) and type http://localhost:8000

**Alternatively**, you can install and run client/server by running the following commands:<br/>
        cd server<br/>
        npm install<br/>
        cd ..\client\my-app<br/>
        npm install<br/>
        npm run build<br/>
        cd ..\\..\server<br/>
        npm start<br/>
        Go to the browser (chrome is preferable) and type http://localhost:8000
        

# The instructions to run the client and the server on MAC/Linux.
1. Run **source start.sh** from the root folder (**liah_waserman_balink**)
2. Wait until batch file will finish to run and server logs will be displayed
3. Go to the browser (chrome is preferable) and type http://localhost:8000

**Alternatively**, you can install and run client/server by running the following commands:<br/>
        cd server<br/>
        npm install<br/>
        cd ../client/my-app<br/>
        npm install<br/>
        npm run build<br/>
        cd ../../server<br/>
        npm start<br/>
        Go to the browser (chrome is preferable) and type http://localhost:8000

